<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Test Niagahoster</title>
	<link rel="stylesheet" href="<?= asset_path('css/bootstrap.bayu.css') ?>">
	<style>
		.top-header-item {
			line-height: 1;
		}

		.top-header-item > .mdi {
			position: relative;
			top: 2px;
		}
		.anchor-close-info:hover{
			text-decoration: none;
			color: unset;
		}
		.anchor-close-info{
			color: unset;
		}
		.ribbon {
			width: 0px;
			height: 35px;
			background-color: transparent;
			position: relative;
			/*position: absolute;
			top: -5px;
			right: 20px;*/
			border: solid 15px #0090EF;
			border-bottom: solid 15px transparent;
			color: #fff;
		}
		.ribbon > .mdi {
			position: absolute;
			left: -7px;
		}
		.php-hosting-feature > li i.mdi-li,
		.php-limit-check{
			color: #00B348;
		}
		.btn-buy-hosting-outline{
			color: #4C4C4C;
			border-color: #4C4C4C;
			border-radius: 50rem;
			font-weight: bold;
		}
		.btn-buy-hosting-outline:hover,
		.btn-buy-hosting-outline.active{
			color: #fff;
			background-color: #008FEE;
			border-color: #008FEE;
		}
		.text-line-through{
			text-decoration: line-through;
		}
		.hosting-package i.mdi-star{
			color: #0090EF;
		}
		.adit{
			background-color: #F4F4F4;
			width: 5rem;
			height: .4rem;
			margin: auto;
		}
		#share-to-sns{
			background-color: #F7F7F7;
		}
		#contact-us{
			background-color: #00A2F3;
			color: #fff;
		}
		#contact-us p {
			font-size: 2em;
		}
		footer{
			background-color: #303030;
			color: #fff;
		}
		a.unstyled {
			color: #fff;
			text-decoration: none;
		}
		a.unstyled:hover, a.unstyled:focus {
			color: #fff;
			text-decoration: none;
		}
		footer h6{
			text-transform: uppercase;
			padding-top: 1rem;
			padding-bottom: 0.85rem;
		}
		.grayed, footer h6{
			color: #808080;
		}
		#newsletter-form{
			background-color: #fff;
		}
		.hosting-package .hosting-package-name{
			font-size: 1.5em;
		}
		.thousand{
			font-size: 2.5em;
		}
		.hosting-package{
			position: relative;
		}
		.hosting-package .feature-list p{
			margin-bottom: 0.25rem;
		}
		.hosting-package.active{
			border-color: #008FEE !important;
		}
		.hosting-package.active .border-bottom{
			border-bottom: 0px !important;
		}
		.hosting-package.active .hosting-package-name,
		.hosting-package.active .pricebox{
			background-color: #008FEE;
			color: #fff;
		}
		.hosting-package.active .registered-user{
			background-color: #007FDE;
			color: #fff;
		}
		.hosting-package.active .btn-buy-hosting-outline{
			color: #fff;
			background-color: #008FEE;
			border: 1px solid #008FEE;
		}

		/*Ribbon*/
		.ribbon-corner {
			width: 150px;
			height: 150px;
			overflow: hidden;
			position: absolute;
		}
		.ribbon-corner::before,
		.ribbon-corner::after {
			position: absolute;
			z-index: -1;
			content: '';
			display: block;
			border: 5px solid #00B359;
		}
		.ribbon-corner span {
			position: absolute;
			display: block;
			width: 225px;
			padding: 15px 0;
			background-color: #00B359;
			box-shadow: 0 5px 10px rgba(0, 0, 0, 0.43);
			color: #fff;
			font: 700 18px/1 'Lato', sans-serif;
			text-shadow: 0 1px 1px rgba(0,0,0,.2);
			text-transform: uppercase;
			text-align: center;
		}

		/* top left*/
		.ribbon-top-left {
			top: -10px;
			left: -10px;
		}
		.ribbon-top-left::before,
		.ribbon-top-left::after {
			border-top-color: transparent;
			border-left-color: transparent;
		}
		.ribbon-top-left::before {
			top: 0;
			right: 0;
		}
		.ribbon-top-left::after {
			bottom: 0;
			left: 0;
		}
		.ribbon-top-left span {
			right: -25px;
			top: 30px;
			transform: rotate(-45deg);
		}
	</style>
	<link rel="stylesheet" href="<?= asset_path('css/material-icons.min.css') ?>">
</head>
<body>
