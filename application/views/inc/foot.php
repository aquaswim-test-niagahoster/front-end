<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script src="<?=base_url('assets/js/jquery-3.2.1.slim.min.js')?>"></script>
<script src="<?=base_url('assets/js/popper.min.js')?>"></script>
<script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5dedca0e845fac00135e321f&product=inline-share-buttons" async="async"></script>
</body>
</html>
