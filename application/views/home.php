<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header class="header-area">
	<div class="top-header border-bottom pb-3 d-none d-lg-block">
		<div class="container d-flex justify-content-between">
			<div>
				<span class="ribbon">
					<div class="mdi mdi-local-offer"></div>
				</span>
				<span class="small pl-2">
				Gratis Ebook 9 Cara Cerdas Menggunakan Domain <a class="anchor-close-info"
																 href="javascript:void(0)">[X]</a>
				</span>
			</div>
			<div class="d-flex justify-content-around">
				<div class="top-header-item p-2">
					<i class="mdi mdi-phone pr-1"></i>
					0272-5305505
				</div>
				<div class="top-header-item p-2">
					<i class="mdi mdi-chat pr-1"></i>
					Live Chat
				</div>
				<div class="top-header-item p-2">
					<i class="mdi mdi-person pr-1"></i>
					Member Area
				</div>
			</div>
		</div>
	</div>
	<div class="main-header border-bottom">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light pl-1 pr-0">
				<a class="navbar-brand" href="<?=site_url()?>">
					<img class="pt-2 pb-2" height="80" src="<?=asset_path('images/niagahoster-logo.png')?>" alt="Niagahoster">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto justify-content-around">
						<?php foreach ([
							'Hosting',
							'Domain',
							'Server',
							'Website',
							'Afiliasi',
							'Promo',
							'Pembayaran',
							'Review',
							'Kontak',
							'Blog',
									   ] as $menu_item): ?>
						<li class="nav-item">
							<a class="nav-link" href="#"><?=$menu_item?></a>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>
<section id="php-hosting" class="border-bottom mb-5">
	<div class="container mt-5 mb-5">
		<div class="row">
			<div class="col-md-6">
				<h1 class="font-weight-bold">PHP Hosting</h1>
				<h3>Cepat, handal, penuh dengan dengan modul PHP yang anda butuhkan</h3>
				<ul class="mdi-ul php-hosting-feature">
					<li><i class="mdi-li mdi mdi-check-circle"></i>Solusi PHP untuk performa query yang cepat</li>
					<li><i class="mdi-li mdi mdi-check-circle"></i>Konsumsi memori yang lebih rendah</li>
					<li><i class="mdi-li mdi mdi-check-circle"></i>Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6, PHP 7</li>
					<li><i class="mdi-li mdi mdi-check-circle"></i>Fitur enkripsi IonCube dan Zend Guard Loaders</li>
				</ul>
			</div>
			<div class="col-md-6">
				<img src="<?=asset_path('svg/illustration banner PHP hosting-01.svg')?>" class="img-fluid">
			</div>
		</div>
	</div>
</section>
<section id="features">
	<div class="container">
		<div class="d-flex flex-column flex-lg-row justify-content-around pb-5">
			<div class="align-self-stretch d-flex flex-column">
				<img src="<?=asset_path('svg/icon PHP Hosting_zendguard.svg')?>" alt="" class="img-fluid flex-fill mb-3">
				<div class="text-center">PHP Zenduard Loader</div>
			</div>
			<div class="align-self-stretch d-flex flex-column">
				<img src="<?=asset_path('svg/icon PHP Hosting_composer.svg')?>" alt="" class="img-fluid flex-fill mb-3">
				<div class="text-center">PHP Composer</div>
			</div>
			<div class="align-self-stretch d-flex flex-column">
				<img src="<?=asset_path('svg/icon PHP Hosting_ioncube.svg')?>" alt="" class="img-fluid flex-fill mb-3">
				<div class="text-center">PHP IonCube Loader</div>
			</div>
		</div>
		<h2 class="text-center font-weight-bold pt-5">Paket Hosting Singapura yang Tepat</h2>
		<h3 class="text-center pb-5">Diskon 40% + Domain dan SSL Gratis Untuk Anda</h3>
		<div class="d-flex flex-column flex-lg-row justify-content-lg-center align-items-stretch align-items-lg-baseline mb-5">
			<?php foreach ($products as $product): ?>
			<div class="hosting-package text-center border <?= @$product['featured']?'active':'' ?>">
				<?php if (@$product['featured']): ?>
					<div class="ribbon-corner ribbon-top-left"><span>Best Seller! </span></div>
				<?endif; ?>
				<div class="hosting-package-name font-weight-bold border-bottom">
					<?=$product['name']?>
				</div>
				<div class="border-bottom pricebox">
					<?php if ($product['discount_price']): ?>
					<p class="indonesia-rp mb-1 text-line-through">
						Rp <?=id_money_format($product['price'])?>
					</p>
					<?php endif; ?>
					<p class="indonesia-rp mb-0">
						<?php
						$price = $product['discount_price'] ? $product['discount_price'] : $product['price'];
						$thausand = (intval($price/1000));
						$thausand_remaining = $price-($thausand*1000);
						?>
						Rp <span><span class="thousand font-weight-bolder"><?= id_money_format($thausand)?></span> <span class="thousand-remain font-weight-bold">.<?= $thausand_remaining?></span> </span> / bln
					</p>
				</div>
				<div class="border-bottom p-1 registered-user">
					<span class="font-weight-bold"><?=id_money_format($product['registered_user'])?></span> Pengguna Terdaftar
				</div>
				<div class="feature-list pr-4 pl-4">
					<?php foreach ($product['features'] as $feature): ?>
					<?=$feature?>
					<?php endforeach; ?>
					<a href="" class="btn btn-buy-hosting-outline mt-5 mb-4">
						<?=isset($product['custom_btn']) ? $product['custom_btn'] : 'Pilih Sekarang'?>
					</a>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
		<h3 class="text-center">Powerful dengan Limit PHP yang lebih besar</h3>
		<div class="d-flex flex-column flex-lg-row justify-content-lg-center mb-4">
			<?php foreach (array_chunk([
				'max_execution_time',
				'max_execution_time',
				'memory_limit',
				'post_max_size',
				'upload_max_filesize',
				'max_input_vars',
			],3) as $php_ini_table): ?>
			<div class="flex-fill p-lg-3">
				<table class="table table-striped mb-0">
					<?php foreach ($php_ini_table as $ini_key): ?>
						<tr><td><i class="mdi mdi-check-circle php-limit-check"></i></td> <td class="text-center"><?=str_replace('_',' ', $ini_key)?> <?=ini_get($ini_key)?></td></tr>
					<?php endforeach; ?>
				</table>
			</div>
			<?php endforeach; ?>
		</div>
		<div class="adit mb-4"></div>
		<h3 class="text-center">Semua Paket Hosting Sudah Termasuk</h3>
		<div class="row">
			<div class="col-lg-4">
				<div class="align-self-stretch d-flex flex-column p-5">
					<img class="img-fluid m-auto" width="100" src="<?=asset_path('svg/icon PHP Hosting_PHP Semua Versi.svg')?>" alt="">
					<h4 class="text-center">PHP Semua Versi</h4>
					<p class="text-center small">Pilih mulai dari versi PHP 5.3 s/d PHP 7.<br>Ubah Sesuka Anda</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="align-self-stretch d-flex flex-column p-5">
					<img class="img-fluid m-auto" width="100" src="<?=asset_path('svg/icon PHP Hosting_My SQL.svg')?>" alt="">
					<h4 class="text-center">PHP Semua Versi</h4>
					<p class="text-center small">Pilih mulai dari versi PHP 5.3 s/d PHP 7.<br>Ubah Sesuka Anda</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="align-self-stretch d-flex flex-column p-5">
					<img class="img-fluid m-auto" width="100" src="<?=asset_path('svg/icon PHP Hosting_CPanel.svg')?>" alt="">
					<h4 class="text-center">PHP Semua Versi</h4>
					<p class="text-center small">Pilih mulai dari versi PHP 5.3 s/d PHP 7.<br>Ubah Sesuka Anda</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="align-self-stretch d-flex flex-column p-5">
					<img class="img-fluid m-auto" width="100" src="<?=asset_path('svg/icon PHP Hosting_garansi uptime.svg')?>" alt="">
					<h4 class="text-center">PHP Semua Versi</h4>
					<p class="text-center small">Pilih mulai dari versi PHP 5.3 s/d PHP 7.<br>Ubah Sesuka Anda</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="align-self-stretch d-flex flex-column p-5">
					<img class="img-fluid m-auto" width="100" src="<?=asset_path('svg/icon PHP Hosting_InnoDB.svg')?>" alt="">
					<h4 class="text-center">PHP Semua Versi</h4>
					<p class="text-center small">Pilih mulai dari versi PHP 5.3 s/d PHP 7.<br>Ubah Sesuka Anda</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="align-self-stretch d-flex flex-column p-5">
					<img class="img-fluid m-auto" width="100" src="<?=asset_path('svg/icon PHP Hosting_My SQL remote.svg')?>" alt="">
					<h4 class="text-center">PHP Semua Versi</h4>
					<p class="text-center small">Pilih mulai dari versi PHP 5.3 s/d PHP 7.<br>Ubah Sesuka Anda</p>
				</div>
			</div>
		</div>
		<div class="adit"></div>
	</div>
	<section id="support-laravel" class="border-bottom">
		<div class="container mt-5">
			<h3 class="text-center mb-5">Mendukung Penuh Framework Laravel</h3>
			<div class="row">
				<div class="col-md-6">
					<p>Tak perlu menggunakan dedicated server ataupun VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda.</p>
					<ul class="mdi-ul php-hosting-feature">
						<li><i class="mdi-li mdi mdi-check-circle"></i>Install laravel <span class="font-weight-bold">1 klik</span> dengan Softcalous Installer</li>
						<li><i class="mdi-li mdi mdi-check-circle"></i>Mendukung ekstensi <span class="font-weight-bold">PHP MCrypt, phar, mbstring, json,</span> dan <span class="font-weight-bold">fileinfo.</span></li>
						<li><i class="mdi-li mdi mdi-check-circle"></i>Tersedia <span class="font-weight-bold">Composer</span> dan <span class="font-weight-bold">SSH</span> untuk menginstal packages pilihan Anda.</li>
					</ul>
					<p class="small">Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis.</p>
					<button class="btn btn-primary btn-lg rounded-pill font-weight-bold">Pilih Hosting Anda</button>
				</div>
				<div class="col-md-6">
					<img src="<?=asset_path('svg/illustration banner support laravel hosting.svg')?>" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section id="php-modules" class="mt-5 mb-5">
		<div class="container">
			<h3 class="text-center">Modul Lengkap untuk Menjalankan Aplikasi PHP Anda</h3>
			<div class="row ml-md-5 mb-5">
				<?php
				$modules = get_loaded_extensions();
				foreach (array_chunk($modules, ceil(count($modules)/4)) as $module_chunk): ?>
					<div class="col-md-3">
						<?php foreach ($module_chunk as $module): ?>
							<?=$module?><br>
						<?php endforeach; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="text-center">
				<button type="button" class="btn btn-lg btn-buy-hosting-outline">Selengkapnya</button>
			</div>
		</div>
	</section>
	<section id="hosting-lve" class="">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h3>Linux Hosting yang Stabil dengan Teknologi LVE</h3>
					<p>SuperMicro <b>Intel Xeon 24-Cores</b> server dengan RAM <b>128 GB</b> dan teknologi <b>LVE CloudLinux</b> untuk stabilitas server Anda. Dilengkapi dengan <b>SSD</b> untuk kecepatan <b>MYSQL</b> dan caching, Apache load balancer berbasis LiteSpeed Technologies, <b>CageFS</b> security, <b>Raid-10</b> protection dan auto backup untuk keamanan website PHP anda.</p>
					<button class="btn btn-primary btn-lg rounded-pill font-weight-bold">Pilih Hoting Anda</button>
				</div>
				<div class="col-md-6">
					<img src="<?=asset_path('images/Image support.png')?>" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section id="share-to-sns" class="p-3">
		<div class="container">
			<div class="row">
				<div class="col-md-6">Bagian jika Anda menyukai halaman ini.</div>
				<div class="col-md-6">
					<div class="sharethis-inline-share-buttons"></div>
				</div>
			</div>
		</div>
	</section>
	<section id="contact-us">
		<div class="container">
			<div class="row pt-5 pb-5">
				<div class="col-md-9">
					<p class="mb-0">Perlu <b>Bantuan?</b> Hubungi Kami: <b>0274-5305505</b></p>
				</div>
				<div class="col-md-3 border-left text-right">
					<button type="button" class="btn btn-lg btn-outline-light rounded-pill font-weight-bold">
						<i class="mdi mdi-chat-bubble"></i>
						Live Chat
					</button>
				</div>
			</div>
		</div>
	</section>
	<footer class="p-5">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h6>Hubungi Kami</h6>
					<p class="small">
						0274-5305505<br>
						Senin-Minggu<br>
						24 Jam Nonstop
					</p>
					<p class="small">
						Jl. Selokan Mataram Monjali <br>
						Karangjati MT I/304 <br>
						Sinduadi, Mlati, Sleman <br>
						Yogyakarta 55284
					</p>
				</div>
				<div class="col-md-3">
					<h6>Layanan</h6>
					<ul class="list-unstyled">
						<li class="small"><a class="unstyled" href="#">Domain</a></li>
						<li class="small"><a class="unstyled" href="#">Shared Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Cloud VPS Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Managed VPS Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Web Builder</a></li>
						<li class="small"><a class="unstyled" href="#">Keamanan SSL/HTTPS</a></li>
						<li class="small"><a class="unstyled" href="#">Jasa Pembuatan Website</a></li>
						<li class="small"><a class="unstyled" href="#">Program Afiliasi</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h6>Service Hosting</h6>
					<ul class="list-unstyled">
						<li class="small"><a class="unstyled" href="#">Domain</a></li>
						<li class="small"><a class="unstyled" href="#">Shared Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Cloud VPS Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Managed VPS Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Web Builder</a></li>
						<li class="small"><a class="unstyled" href="#">Keamanan SSL/HTTPS</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h6>Tutorial</h6>
					<ul class="list-unstyled">
						<li class="small"><a class="unstyled" href="#">Domain</a></li>
						<li class="small"><a class="unstyled" href="#">Shared Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Cloud VPS Hosting</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<h6>Tentang Kami</h6>
					<ul class="list-unstyled">
						<li class="small"><a class="unstyled" href="#">Domain</a></li>
						<li class="small"><a class="unstyled" href="#">Shared Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Cloud VPS Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Managed VPS Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Web Builder</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h6>Kenapa Pilih Niagahoster</h6>
					<ul class="list-unstyled">
						<li class="small"><a class="unstyled" href="#">Domain</a></li>
						<li class="small"><a class="unstyled" href="#">Shared Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Cloud VPS Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Managed VPS Hosting</a></li>
						<li class="small"><a class="unstyled" href="#">Web Builder</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h6>Newsletter</h6>
					<form id="newsletter-form" class="p-1 rounded-pill">
						<div class="row m-0">
							<div class="col p-0 d-flex">
								<input type="text" style="background-color: transparent;" class="form-control border-0" placeholder="Email">
							</div>
							<div class="col p-0">
								<button type="submit" class="btn btn-primary rounded-pill">Berlangganan</button>
							</div>
						</div>
					</form>
					<p class="small">
						Dapatkan promo dan konten menarik <br>
						dari penyedia hosting terbaik Anda.
					</p>
				</div>
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-4">
							<button type="button" class="btn btn-outline-light rounded-circle btn-lg">
								<i class="mdi mdi-widgets"></i>
							</button>
						</div>
						<div class="col-md-4">
							<button type="button" class="btn btn-outline-light rounded-circle btn-lg">
								<i class="mdi mdi-widgets"></i>
							</button>
						</div>
						<div class="col-md-4">
							<button type="button" class="btn btn-outline-light rounded-circle btn-lg">
								<i class="mdi mdi-widgets"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h6>Pembayaran</h6>
					<div class="mb-3">
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
						<button type="button" class="btn btn-outline-light rounded-pill" disabled>Pay</button>
					</div>
					<p class="small grayed">Aktifasi instan dengan e-payment. Hosting dan domain langsung aktif</p>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-9">
					<p class="small">
						Copyright &copy;2016 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovilagge Jakarta
						<br>
						Cloud VPS Murah powered by Webuzo Softcalous, Intel SSD and cloud computing technology
					</p>
				</div>
				<div class="col-md-3 small">
					<a href="" class="unstyled">Syarat dan ketentuan</a> | <a href="" class="unstyled">Kebijakan Privasi</a>
				</div>
			</div>
		</div>
	</footer>
</section>
