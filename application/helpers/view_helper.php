<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('asset_path')) {
	function asset_path($path='') {
		return base_url('assets/'.$path);
	}
}

if (!function_exists('id_money_format')) {
	function id_money_format($number){
		return number_format($number, 0, ',', '.');
	}
}
