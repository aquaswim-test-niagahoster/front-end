<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class MY_controller
 * @property CI_Parser parser
 */
class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('parser');
	}

	public function render($view, $data = [])
	{
		$this->load->view('inc/head', $data);
		$this->parser->parse($view,$data);
		$this->load->view('inc/foot', $data);
	}
}
